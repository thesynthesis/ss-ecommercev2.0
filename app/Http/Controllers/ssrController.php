<?php

namespace App\Http\Controllers;


use App\User;
use Input;
use DB;
use View;
use App\Products;
use Validator;
use App\Http\Requests;
use Illuminate\Http\Request;
use Illuminate\Support\Collection;
use App\Http\Controllers\Controller;

class ssrController extends Controller
{
	 public function __construct()
    {
        $this->middleware('auth');
    }

	 public function index($id)
    {
    	$idd = $id;
    	$products = Products::lists('prodDesc', 'id');
        return View::make('navigation/servicerequest', compact('products', $products), compact('id', $idd));
    }

	public function insert(Request $request)
	{
		
		$post = $request->all();
		$v = Validator::make($request->all(),
			[
				'ssrName' => 'required',
				'ssrLocation' => 'required',
				'ssrContact' => 'required',
				'ssrProblem' => 'required',
				'ssrServicereq' => 'required',
		
			]
		);

		if($v->fails())
		{
			return redirect()->back()->withErrors($v->errors());
		}
		else
		{
			$data = array(
					'ssrName' => $post['ssrName'],
					'ssrLocation' => $post['ssrLocation'],
					'ssrContact' => $post['ssrContact'],
					'ssrProblem' => $post['ssrProblem'],
					'ssrServicereq' => $post['ssrServicereq'],
					'custName' => $post['userid'],
				    'prod' => $request->product,
				    'id' => $post['id'],
			
				);
			$userid = Input::get('userid');
			$act = Input::get('actType');
			$result = DB::table('users')->where('acctUsername', $userid)->pluck('id');
			$result2 = $result[0];
			
			
			$ch = DB::table('tblssr')->insert($data);
			DB::insert( DB::raw("INSERT INTO tblactivity(actType, id, username) VALUES ('$act','$result2', '$userid')"));
			if($ch > 0)
			{
				$id = $request->prod;
				$product = Products::find($id);
 
				return redirect()->route('payPremium', compact('product', $product));
			}
		}
	}
}