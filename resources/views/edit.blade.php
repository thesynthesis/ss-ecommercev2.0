

@extends('layouts.nav')

@section('content')
  <div class="content-wrapper">
    <section class="content-header">
        <h1>
          Edit {{$user->acctName}}'s Profile</h2>
        <small>Fill out those boxes completely</small>
        </h1>

      </section>

     <section class="content">
       <div class="row">
             <!-- left column -->

             <div class="col-md-10 col-md-offset-1">
               <!-- general form elements -->
               <div class="box box-primary">


{!! Form::model($user,
  [
  'method' => 'patch',
  'route' => ['userupdate', $user->id],
  'class' => 'form-group',
  ]); !!}

  <div class="form-group">
     <div class="col-md-3">
      {!! Form::label('name', 'Name:') !!}
     {!! Form::text('name', $user->acctName, ['class' => 'form-control']) !!}

   </div>
  </div>

  <div class="form-group">
 <div class="col-md-3">

    {!! Form::label('username', 'Username:') !!}
     {!! Form::text('username', $user->acctUsername, ['class' => 'form-control']) !!}
   </div>
  </div>
  <div class="form-group">
<div class="col-md-3">
  {!! Form::label('contact', 'Contact No:') !!}
     {!! Form::text('contact', $user->acctContact, ['class' => 'form-control']) !!}
   </div>
  </div>

  <div class="form-group">
<div class="col-md-3">
  {!! Form::label('email', 'E-Mail Address:') !!}
     {!! Form::text('email', $user->email, ['class' => 'form-control']) !!}
   </div>
  </div>

  <div class="form-group">
      <div class="col-sm-3">
<br>
        {!! Form::submit('Edit', ['class' => 'btn btn-primary  btn-flat ']) !!}
        {!! Form::close() !!}
    </div>
  </div>


</div>


</div>
</section>
</div>


@endsection
